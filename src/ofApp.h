#pragma once

#include "ofMain.h"
#include "ofxRunway.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp, public ofxRunwayListener{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		
		//give feedback from Runway
		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);

		//control listeners
		void changeSourcePressed();	//defines button function
		void changeTargetPressed();	//defines button function

		//the heavy lifters
		void getFace(ofImage& output);	//gets a face from faceGenerator
		void swapFace();				//gets result from faceSwapper

		//Runway variables
		ofxRunway faceGenerator;	//StyleGAN2
		ofxRunway faceSwapper;		//Few-Shot-Face-Translation-GAN
		ofxRunwayData data;			//encapsulates input for Runway
		vector<float> seed;			//input for faceGenerator
		ofImage face1;				//source image for faceSwapper
		ofImage face2;				//target image for faceSwapper
		ofImage result;				//result image from faceSwapper

		//controls
		ofxPanel gui;				//container
		ofxButton changeSource;
		ofxButton changeTarget;
};
