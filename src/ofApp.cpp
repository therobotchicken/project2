#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//setup and start models
	faceGenerator.setup(this, "http://localhost:8000");
	faceSwapper.setup(this, "http://localhost:8001");
	faceGenerator.start();
	faceSwapper.start();

	//initialize the source, target, and resulting face
	getFace(face1);
	getFace(face2);
	swapFace();

	//setup GUI controls
	gui.setup();
	gui.add(changeSource.setup("Change Source"));
	gui.add(changeTarget.setup("Change Target"));
	//listeners
	changeSource.addListener(this, &ofApp::changeSourcePressed);
	changeTarget.addListener(this, &ofApp::changeTargetPressed);
}

//--------------------------------------------------------------
void ofApp::update(){
}

//--------------------------------------------------------------
void ofApp::draw(){
	//draw faces side by side
	face1.draw(0, 0, 512, 512);
	result.draw(512, 0, 512, 512);
	face2.draw(1024, 0, 512, 512);

	//draw labels
	ofDrawBitmapStringHighlight("Source", 0, 512);
	ofDrawBitmapStringHighlight("Result", 512, 512);
	ofDrawBitmapStringHighlight("Target", 1024, 512);

	//draw controls
	gui.draw();
}

//listener for changeSource button
//-clear() functions fulfill while loop condition for getFace() and swapFace()
void ofApp::changeSourcePressed()
{
	face1.clear();
	getFace(face1);
	result.clear();
	swapFace();
}

//listener for changeTarget button
//-clear() functions fulfill while loop condition for getFace() and swapFace()
void ofApp::changeTargetPressed()
{
	face2.clear();
	getFace(face2);
	result.clear();
	swapFace();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//For some reason, changing both pictures with one button causes the program to freeze.

	//change source picture
	//-clear() functions fulfill while loop condition for getFace() and swapFace()
	if (key == OF_KEY_LEFT)
	{
		face1.clear();
		getFace(face1);
		result.clear();
		swapFace();
	}

	//change target picture
	//-clear() functions fulfill while loop condition for getFace() and swapFace()
	if (key == OF_KEY_RIGHT)
	{
		face2.clear();
		getFace(face2);
		result.clear();
		swapFace();
	}
}

void ofApp::runwayInfoEvent(ofJson& info)
{
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}

void ofApp::runwayErrorEvent(string& message)
{
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}

//generates a face from StyleGAN2 and puts it in an ofImage
//-I found found it important that it take an ofImage address as a parameter
void ofApp::getFace(ofImage& output)
{
	//clear and repopulate vector float
	if (seed.size() != 0)
		seed.clear();
	for (int i = 0; i < 512; i++)
	{
		seed.push_back(ofRandomf());
	}

	//generate face and put in output ofImage
	if (!faceGenerator.isBusy())
	{
		data.setFloats("z", seed);
		data.setFloat("truncation", 0.8);
		faceGenerator.send(data);
	}
	while (!output.isAllocated())
	{
		faceGenerator.get("image", output);
	}
}

//sends both faces to Few-Shot-Face-Translation-GAN and gets the result back
void ofApp::swapFace()
{
	if (!faceSwapper.isBusy())
	{
		data.setImage("source", face1.getPixels(), OFX_RUNWAY_JPG);
		data.setImage("target", face2.getPixels(), OFX_RUNWAY_JPG);
		faceSwapper.send(data);
	}
	while (!result.isAllocated())
	{
		faceSwapper.get("result", result);
	}
}