This program works using openFrameworks in combination with RunwayML to transplant one person's face on to the head of another, kind of like the movie Face/Off. This is done by first generating two faces in the StyleGAN2 model and then sending them as input to Few-Shot-Face-Translation-GAN.

In testing, many transplants were subtle, but many others are much more noticeable. Some people look years younger, and some women grow beards.
![example run](/example1.JPG)

Others gave creepier results.
![example run](/example2.JPG)

[Here's an demonstration video.](https://youtu.be/NBZwoaUgF4w "demo")

There are two buttons in the top left of the window. The top button "Change Source" will change the picture on the left. The bottom one "Change Target" will change the right picture. Both buttons will cause a new picture to be generated in the middle. Alternatively, the arrow keys can be used. The left arrow will change the source, and right will change the target.

This can be a bit fickle. First, make sure StyleGAN2 is set to generate faces, as it has many generation options. Then, run StyleGAN2 first. Then, run Few-Shot-Face-Translation-GAN. This one will probably take a while to start, long enough for StyleGAN2 to stop automatically. To prevent this, move the horizontal scroll bar in StyleGAN2's HTTP source box around frequently. When both models are running, start the program, moving the horizontal scroll bar as needed.

Changing faces may take a few pictures between inputs.

An issue that started to pop up late in testing was an error reporting an HTTP response error. This seems to be out of my control. If this happens, close the program and rerun it before either of the models stops.